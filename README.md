# README #

Aplicación desarrollada en Python la cual calcula la serie de Fibonacci, mediante consola o de forma gráfica el usuario deberá ingresar el número de iteraciones desea calcular de la serie y mostrar la serie completa hasta dicho momento en forma lineal. Ej. N° de Iteraciones: 5 - Resultado esperado: “1, 2, 3, 5, 7”

### Autor ###

* Enlace del repositorio: https://jorge_barrera@bitbucket.org/jorge_barrera/fibonacci.git
* Nombre Completo: Jorge Enrique Barrera Salgado
* Código Uniandes: 201510887
* Cuenta uniandes: je.barrera11@uniandes.edu.co