print ("     _____ ___ ____   ___  _   _    _    ____ ____ ___ ");
print ("    |  ___|_ _| __ ) / _ \| \ | |  / \  / ___/ ___|_ _|");
print ("    | |_   | ||  _ \| | | |  \| | / _ \| |  | |    | | ");
print ("    |  _|  | || |_) | |_| | |\  |/ ___ | |__| |___ | | ");
print ("    |_|   |___|____/ \___/|_| \_/_/   \_\____\____|___|");
print ("            UNIANDES - JORGE BARRERA - 2016");
print ("");


# Funcion que calcula de forma recursiva la serie de Fibonacci
def calcularFibonacci(actual, anterior, iteracionesCalculadas, cantidadIteraciones, serieCompleta):
    # Acumula el numero de iteraciones
    iteracionesCalculadas = iteracionesCalculadas + 1

    # Suma el resultado actual y anterior de la serie
    resultado = actual + anterior;

    # Contruye la cadena con el resultado de la serie
    serieCompleta = serieCompleta + ("%s, " % resultado);

    if iteracionesCalculadas == cantidadIteraciones:
        # Imprime el resultado de la serie
        print("");
        print(("No de Iteraciones: %s - Resultado esperado: " % cantidadIteraciones) + serieCompleta);
    else:
        # Sigue calculando la siguiente iteracion de la serie
        calcularFibonacci(resultado, actual, iteracionesCalculadas, cantidadIteraciones, serieCompleta);


# Inicializacion de paramentros
actual = 1;
anterior = 0;
iteracionesCalculadas = 0;
serieCompleta = "";
cantidadIteraciones = eval(raw_input("Digite el numero de iteraciones que desea calcular de la serie: _"));

# Ejecuta el calculo de forma recursiva la serie de Fibonacci
calcularFibonacci(actual, anterior, iteracionesCalculadas, cantidadIteraciones, serieCompleta);
